from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from .views import logIn, signUp
from .apps import Story9Config

class UnitTestonStory9(TestCase):
   
    def setUp(self):
        self.client = Client()
        self.logIn_url = reverse("story9:login")
        self.signUp_url = reverse("story9:signup")
        self.logOut_url = reverse("story9:logout")

    # LOG IN PAGE TEST
    def test_url(self):
        response = self.client.get(self.logIn_url)
        self.assertEquals(response.status_code, 200)

    def test_template_yang_digunakan(self):
        response = self.client.get(self.logIn_url)
        self.assertTemplateUsed(response, 'login.html')

    def test_check_function_yang_digunakan(self):
        found = resolve(self.logIn_url)
        self.assertEqual(found.func, logIn)
    
    def test_check_halaman_login_ada_form(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)

    def test_login_POST(self):
        response = self.client.post(self.logIn_url, follow=True, data={
            'username': 'dinar',
            'password': 'pinter'
        })
        self.assertContains(response, 'dinar')

    def test_signin_header(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertContains(response, "Sign in")
        self.assertContains(response, "Username")
        self.assertContains(response, "Password")
        self.assertContains(response, "Don't have an account")

    # SIGN UP PAGE TEST
    def test_signup_url(self):
        response = self.client.get(self.signUp_url)
        self.assertEquals(response.status_code, 200)

    def test_template_yang_digunakan_signup(self):
        response = self.client.get(self.signUp_url)
        self.assertTemplateUsed(response, 'signup.html')

    def test_check_function_signup(self):
        found = resolve(self.signUp_url)
        self.assertEqual(found.func, signUp)

    def test_signup_POST(self):
        response = self.client.post(self.signUp_url, follow=True, data={
            'username': 'dinar',
            'password': 'pinter'
        })
        self.assertContains(response, 'dinar')

    def test_signup_header(self):
        response = self.client.get(self.signUp_url)
        self.assertContains(response, "Create a new account")


    #SIGN OUT TEST
    def test_apakah_signout_berhasil(self):
        response = self.client.get(self.logOut_url)
        self.assertEqual(response.status_code, 302)
