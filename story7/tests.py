from django.test import TestCase, Client
from django.urls import reverse

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.story7_url = reverse("story7:accordion")
    
    def test_url(self):
        response = self.client.get(self.story7_url)
        self.assertEquals(response.status_code, 200)

    def test_template(self):
        response = self.client.get(self.story7_url)
        self.assertTemplateUsed(response, 'accordion.html')

    def test_konten(self):
        response = self.client.get(self.story7_url)
        self.assertContains(response, "ACTIVITY")
        self.assertContains(response, "EXPERIENCE")
        self.assertContains(response, "ACHIEVEMENT")
        self.assertContains(response, "HOBBY")