from django.contrib import admin
from django.urls import path,include
from . import views

app_name = 'story7'

urlpatterns = [
    path('story7/', views.accordion, name='accordion'),
]