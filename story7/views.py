from django.shortcuts import render
from django.http import HttpResponse

def accordion(request):
     return render(request, 'accordion.html')