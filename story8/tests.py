from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from .views import searchbook, data
from .apps import Story8Config
import unittest

class UnitTest(TestCase):
    
    def setUp(self):
        self.client = Client()
        self.story8_url = reverse("story8:searchbook")

    def test_url(self):
        response = self.client.get(self.story8_url)
        self.assertEquals(response.status_code, 200)

    def test_template_yang_digunakan(self):
        response = self.client.get(self.story8_url)
        self.assertTemplateUsed(response, 's8-book.html')

    def test_apakah_mengakses_function_yang_benar(self):
        found = resolve("/story8/")
        self.assertEqual(found.func, searchbook)
    
    def test_apakah_mengakses_function_page_json_yang_benar(self):
        found = resolve("/story8/data/")
        self.assertEqual(found.func, data)
